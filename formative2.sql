INSERT INTO address VALUES 
(NULL, "Jl. Pahlawan No 17", "Jakarta","DKI Jakarta","Indonesia",6113),
(NULL, "Jl. Super No 45", "Bogor","Jawa Barat","Indonesia",16113);

INSERT INTO Manufacturer VALUES 
(NULL, "Luiv", 1),
(NULL, "Victorya",2);

INSERT INTO Brand VALUES 
(NULL, "JuMi"),
(NULL, "NaKo");

INSERT INTO Valuta VALUES 
(NULL, "IDR", "Rupiah"),
(NULL, "USD", "Dollar");

INSERT INTO Product VALUES 
(NULL, 141, "Strawberry Juice", "Minuman Jus Susu Rasa Stoberi", 2,1,10),
(NULL, 142, "Orange Juice", "Minuman Jus Susu Rasa Jeruk", 2,1,0),
(NULL, 143, "Manggo Juice", "Minuman Jus Susu Rasa Mangga", 2,1,2),
(NULL, 144, "Lychee Juice", "Minuman Jus Susu Rasa Leci", 2,1,0),
(NULL, 145, "Grape Juice", "Minuman Jus Susu Rasa Anggur", 2,1,4),
(NULL, 152, "Oky Crunch", "Cereal Cokelat Berbentuk Lingkaran", 1,2,10),
(NULL, 153, "Yummy Dim", "Dimsum yang Dikemas Dengan Baik", 1,2,4),
(NULL, 154, "Jelly Putter", "Minuman Susu dengan Jelly dengan Rasa Salad Buah", 1,2,20),
(NULL, 155, "Fummy Yummy", "Kue berbentuk seperti Jamur dengan rasa Vanilla", 1,2,15),
(NULL, 156, "Wildly Crack", "Kerupuk Pedas dengan berbagai level", 1,2,1);

INSERT INTO price VALUES 
(NULL,1,1, 13000),
(NULL,1,2, 1),
(NULL,2,1, 26000),
(NULL,2,2, 2),
(NULL,3,1, 26000),
(NULL,3,2, 2),
(NULL,4,1, 13000),
(NULL,4,2, 1),
(NULL,5,1, 26000),
(NULL,5,2, 1),
(NULL,6,1, 13000),
(NULL,6,2, 1),
(NULL,7,1, 26000),
(NULL,7,2, 2),
(NULL,8,1, 13000),
(NULL,8,2, 1),
(NULL,9,1, 26000),
(NULL,9,2, 2),
(NULL,10,1, 39000),
(NULL,10,2, 3);